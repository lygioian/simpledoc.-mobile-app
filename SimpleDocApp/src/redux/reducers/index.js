import { combineReducers } from 'redux';

import me from './me';

export default function createRootReducer() {
    return combineReducers({
        me,
    });
}
