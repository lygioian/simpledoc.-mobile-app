// import { bindActionCreators } from 'redux';
// import { createAction } from 'redux-actions';
// import { createActionThunk } from 'redux-thunk-actions';
// import _ from 'lodash';

// import AsyncStorage from '@react-native-community/async-storage';
// import { axios } from '../../lib/custom-axios';
// import ImagePicker from "react-native-image-picker";
// import ImageEditor from "@react-native-community/image-editor";
// import { MeAPI } from '../../api/me';
// import { UsersAPI } from '../../api/users';

// import { EVENTS_PAGE_SIZE, AUTH_URL } from '../../config';

// import {
//     TOKEN,
//     SET_TOKEN,
//     RESET_TOKEN,
//     GET_PROFILE,
//     GET_PROFILE_SUCCEEDED,
//     GET_MY_EVENTS_LATEST,
//     GET_MY_EVENTS_SAVED,
//     GET_MY_FOLLOWERS,
//     GET_MY_FOLLOWING,
//     SET_CREATING_BUNDLE_STATE,
//     LOG_OUT
// } from '../action-types';
// // import { FacebookSDK } from '../../lib/wrapper/facebook';
// // import { setCookie, removeCookie } from '../../lib/storage';
// // import { BundlesAPI } from '../../api/bundles';

// const logout = createAction(LOG_OUT, () => ({
// }))

// const storeToken = createAction(SET_TOKEN, (token) => ({
//     token
// }))
// const storeInfo = createAction(GET_PROFILE_SUCCEEDED, (data) => ({
//     ...data
// }))
// export const setToken = async (token) => {
//     try {
//       await AsyncStorage.setItem(TOKEN, JSON.stringify(token));
//       Promise.resolve();
//     } catch (error) {
//       // Error saving data
//     }
//   };
//   export const resetToken = async (token) => {
//     try {
//     //   await AsyncStorage.removeItem(TOKEN);
//       await AsyncStorage.clear();
//     } catch (error) {
//       // Error saving data
//     }
//   };

// export const login = (username, password) => async (dispatch, getState) => {
//     return new Promise(async (resolve, reject) => {
//         try {
//             const {data}  = await MeAPI.login(username, password);

//             if(data.error){
//                 reject(data);
//             }
//             const { token } = data;
//             console.log(token)
//             setToken({token: token});
//             dispatch(storeToken(token));
//             dispatch(fetchPersionalInfoAction(token));
//             resolve(data);
//         } catch (err) {
//             reject(err.response);
//         }
//     });
// };

// export const getUserById = (userId) => async (dispatch, getState) => {
//     return new Promise(async (resolve, reject) => {
//         try {
//             const {data}  = await MeAPI.getUserById(userId);
//             if(data.error){
//                 reject(data);
//             }
//             resolve(data);
//         } catch (err) {
//             reject(err.response);
//         }
//     });
// };

// export const getUserByKeyword = (keyword) => async (dispatch, getState) => {
//     return new Promise(async (resolve, reject) => {
//         try {
//             const {data}  = await MeAPI.getUserByKeyword(keyword);
//             console.log(data)
//             if(data.error){
//                 reject(data);
//             }
//             resolve({data: data});
//         } catch (err) {
//             reject(err.response);
//         }
//     });
// };

// export const signup = (username, password) => async (dispatch, getState) => {
//     return new Promise(async (resolve, reject) => {
//         try {
//             const {data}  = await UsersAPI.signUp({
//                 username: username,
//                 password: password,
//                 role: 'user',
//                 name: {
//                     firstName: username,
//                     lastName: username
//                 },
//             });
//             if(data.error){
//                 reject(data);
//             }
//             const { token } = data.data;
//             dispatch(storeToken(token));
//             dispatch(fetchPersionalInfoAction(token));
//             resolve(data);
//         } catch (err) {
//             reject(err.response);
//         }
//     });
// };

// export const updateProfile = (data) => async (dispatch, getState) => {
//     try {
//         console.log(data); 
//         const res = await MeAPI.updateProfile(data);
//         dispatch(fetchPersionalInfoAction());
//         return Promise.resolve(res.data);
//     } catch(err) {
//         console.log(err);
//         return Promise.reject(err);
//     }
    
// };


// export const fetchPersionalInfoAction = (token = null) => async (
//     dispatch,
//     getState
// ) => {
//     try {
//         // if(!token){
//         //     token = getState().me.token;
//         // }
//         const {data} = await MeAPI.getProfile();
//         if(data != undefined){
//             // savePersionalInfo(data);
//             dispatch(storeInfo(data));
//             return Promise.resolve();
//         }

//     } catch(err){
//         console.log(err);
//     }
// }

// export const loadLocal = () => async (dispatch, getState) => {
//     const savedData = await AsyncStorage.getItem(TOKEN);
//     console.log("save", savedData)
//     if (!savedData) {
//         Promise.reject();
//     }
//     console.log("Load local")
//     console.log(savedData) 
//     const {token} = JSON.parse(savedData);
//     console.log(token);
//     if(token){
//         dispatch(storeToken(token));
//         dispatch(fetchPersionalInfoAction(token));
//         Promise.resolve();
//     }
// }

// export const deleteUserDataAction = (token, data) => async (dispatch, getState) => {
//     try {
//         await AsyncStorage.removeItem(TOKEN);
//         dispatch(logout());
//         Promise.resolve();
//     } catch (error) {
//         Promise.reject(error);
//     }
// }

// // export const logout = () => async (dispatch, getState) => {
// //     dispatch(resetToken());
// // };

// export const changePassword = (oldPassword, newPassword) => async (dispatch, getState) => {
//     return new Promise(async (resolve, reject) => {
//         try {
//             console.log("You get A")
//             console.log(oldPassword)
//             console.log(newPassword)
//             const {data}  = await MeAPI.changePassword(oldPassword, newPassword);
//             console.log("You get B")
//             if(data.error){
//                 reject(data);
//             }
//             resolve(data);
//         } catch (err) {
//             reject(err.response);
//         }
//     });
// }

// export const changeProfilePhotoAction = (data) => async (dispatch, getState) => {
//     const res = await MeAPI.updateProfilePicture(data);
//     return Promise.resolve(res.data);
// };

// export const meActions = {
//     login,
//     loadLocal,
//     changeProfilePhoto: changeProfilePhotoAction,
//     signup,
//     updateProfile,
//     getUserById,
//     getUserByKeyword,
//     deleteUserDataAction,
//     changePassword
// };

// export function bindMeActions(currentActions, dispatch) {
//     return {
//         ...currentActions,
//         meActions: bindActionCreators(meActions, dispatch),
//     };
// }
