// dummy
export const DUMMY_SET = 'dummy/DUMMY_SET';
export const DUMMY_ADD_DELAY = 'dummy/DUMMY_ADD_DELAY';
export const DUMMY_ADD_DELAY_SUCCEEDED = `${DUMMY_ADD_DELAY}_SUCCEEDED`;

export const GET_CUSTOMER_SUSSCESS = 'user/GET_CUSTOMER_SUSSCESS';

export const GET_ADMINS = 'superadmins/GET_ADMINS';
export const GET_ADMINS_SUCCEEDED = `${GET_ADMINS}_SUCCEEDED`;

// me
export const SET_TOKEN = 'me/SET_TOKEN';
export const RESET_TOKEN = 'me/RESET_TOKEN';
export const TOKEN = 'me/TOKEN';
export const SET_CREATING_BUNDLE_STATE = 'me/SET_CREATING_BUNDLE_STATE';
export const GET_PROFILE = 'me/GET_PROFILE';
export const GET_PROFILE_SUCCEEDED = `${GET_PROFILE}_SUCCEEDED`;
export const GET_MY_EVENTS_LATEST = 'me/GET_MY_EVENTS_LATEST';
export const GET_MY_EVENTS_LATEST_SUCCEEDED = `${GET_MY_EVENTS_LATEST}_SUCCEEDED`;
export const GET_MY_EVENTS_SAVED = 'me/GET_MY_EVENTS_SAVED';
export const GET_MY_EVENTS_SAVED_SUCCEEDED = `${GET_MY_EVENTS_SAVED}_SUCCEEDED`;
export const GET_MY_FOLLOWING = 'me/GET_MY_FOLLOWING';
export const GET_MY_FOLLOWING_SUCCEEDED = `${GET_MY_FOLLOWING}_SUCCEEDED`;
export const GET_MY_FOLLOWERS = 'me/GET_MY_FOLLOWERS';
export const GET_MY_FOLLOWERS_SUCCEEDED = `${GET_MY_FOLLOWERS}_SUCCEEDED`;

// Event
export const GET_NEWEST_EVENTS = 'EVENTS/GET_NEWEST_EVENTS';
export const GET_EVENT_USER_STAFF = 'EVENTS/GET_EVENT_USER_STAFF';
export const GET_EVENT_USER_CREATED = 'EVENTS/GET_EVENT_USER_CREATED';
export const GET_EVENT_USER_REGISTERED = 'EVENTS/GET_EVENT_USER_REGISTERED'
export const GET_EVENT_LEAD = 'EVENTS/GET_EVENT_LEAD';

export const GET_NEWEST_EVENTS_SUCCEEDED = `${GET_NEWEST_EVENTS}_SUCCEEDED`;
export const GET_EVENT_LEAD_SUCCEEDED = `${GET_EVENT_LEAD}_SUCCEEDED`;
export const GET_NEWEST_EVENTS_STARTED = `${GET_NEWEST_EVENTS}_STARTED`;

export const GET_NEWSFEED_EVENTS = 'EVENTS/GET_NEWSFEED_EVENTS';
export const GET_NEWSFEED_EVENTS_SUCCEEDED = `${GET_NEWSFEED_EVENTS}_SUCCEEDED`;
export const GET_NEWSFEED_EVENTS_STARTED = `${GET_NEWSFEED_EVENTS}_STARTED`;

export const GET_DISCOVER_EVENTS = 'EVENTS/GET_DISCOVER_EVENTS';
export const GET_DISCOVER_EVENTS_SUCCEEDED = `${GET_DISCOVER_EVENTS}_SUCCEEDED`;
export const GET_DISCOVER_EVENTS_STARTED = `${GET_DISCOVER_EVENTS}_STARTED`;

export const GET_RELEVANT_EVENTS = 'EVENTS/GET_RELEVANT_EVENTS';
export const GET_RELEVANT_EVENTS_SUCCEEDED = `${GET_RELEVANT_EVENTS}_SUCCEEDED`;
export const GET_RELEVANT_EVENTS_STARTED = `${GET_RELEVANT_EVENTS}_STARTED`;

export const GET_BUNDLE = 'EVENTS/GET_BUNDLE';
export const GET_BUNDLE_SUCCEEDED = `${GET_BUNDLE}_SUCCEEDED`;

export const GET_EVENTS_BY_KEYWORD = 'EVENTS/GET_EVENTS_BY_KEYWORD';
export const GET_EVENTS_BY_KEYWORD_SUCCEEDED = `${GET_EVENTS_BY_KEYWORD}_SUCCEEDED`;
export const GET_EVENTS_BY_KEYWORD_STARTED = `${GET_EVENTS_BY_KEYWORD}_STARTED`;

export const UPDATE_ACTIVE_BUNDLE_STATUS =
  'EVENTS/UPDATE_ACTIVE_BUNDLE_STATUS';

// user
export const GET_USER_INFORMATION = 'users/GET_USER_INFORMATION';
export const GET_USER_INFORMATION_SUCCEEDED = `${GET_USER_INFORMATION}_SUCCEEDED`;
export const GET_USER_EVENTS = 'users/GET_USER_EVENTS';
export const GET_USER_EVENTS_SUCCEEDED = `${GET_USER_EVENTS}_SUCCEEDED`;
export const GET_USER_FOLLOWERS = 'users/GET_USER_FOLLOWERS';
export const GET_USER_FOLLOWERS_SUCCEEDED = `${GET_USER_FOLLOWERS}_SUCCEEDED`;
export const GET_USER_FOLLOWING = 'users/GET_USER_FOLLOWING';
export const GET_USER_FOLLOWING_SUCCEEDED = `${GET_USER_FOLLOWING}_SUCCEEDED`;
export const SET_USER_EVENTS_LOADING = 'users/SET_USER_EVENTS_LOADING';

// News
export const GET_NEWEST_NEWS = 'NEWS/GET_NEWEST_NEWS';
export const GET_NEWEST_NEWS_SUCCEEDED = `${GET_NEWEST_NEWS}_SUCCEEDED`;
export const GET_NEWEST_NEWS_STARTED = `${GET_NEWEST_NEWS}_STARTED`;

export const GET_NEWSFEED_NEWS = 'NEWS/GET_NEWSFEED_NEWS';
export const GET_NEWSFEED_NEWS_SUCCEEDED = `${GET_NEWSFEED_NEWS}_SUCCEEDED`;
export const GET_NEWSFEED_NEWS_STARTED = `${GET_NEWSFEED_NEWS}_STARTED`;

export const GET_DISCOVER_NEWS = 'NEWS/GET_DISCOVER_NEWS';
export const GET_DISCOVER_NEWS_SUCCEEDED = `${GET_DISCOVER_NEWS}_SUCCEEDED`;
export const GET_DISCOVER_NEWS_STARTED = `${GET_DISCOVER_NEWS}_STARTED`;

export const GET_RELEVANT_NEWS = 'NEWS/GET_RELEVANT_NEWS';
export const GET_RELEVANT_NEWS_SUCCEEDED = `${GET_RELEVANT_NEWS}_SUCCEEDED`;
export const GET_RELEVANT_NEWS_STARTED = `${GET_RELEVANT_NEWS}_STARTED`;

export const GET_NEWS = 'NEWS/GET_NEWS';
export const GET_NEWS_SUCCEEDED = `${GET_NEWS}_SUCCEEDED`;

export const GET_NEWS_BY_KEYWORD = 'NEWS/GET_NEWS_BY_KEYWORD';
export const GET_NEWS_BY_KEYWORD_SUCCEEDED = `${GET_NEWS_BY_KEYWORD}_SUCCEEDED`;
export const GET_NEWS_BY_KEYWORD_STARTED = `${GET_NEWS_BY_KEYWORD}_STARTED`;

export const UPDATE_ACTIVE_NEWS_STATUS =
  'NEWS/UPDATE_ACTIVE_NEWS_STATUS';

//Loading action types
export const SHOW_LOADING = 'loading/SHOW'
export const HIDE_LOADING = 'loading/HIDE'

//Toast notification action types
export const SHOW_NOTIFICATION = 'toasting/SHOW'
export const HIDE_NOTIFICATION = 'toasting/HIDE'

//Dialog
export const SHOW_DIALOG = 'dialog/SHOW'
export const HIDE_DIALOG = 'dialog/SEND'

export const LOG_OUT = 'LOG_OUT'