import React, { Component } from 'react';
import PropTypes, { element } from 'prop-types';

import {
  StyleSheet,
  View,
  Image,
  Dimensions,
  ScrollView,
  TouchableOpacity
} from 'react-native';
import Text from '../../components/Text'


const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

import About_Us from './assets//code.js'
import Log_Out from './assets//log-out.js'
import Notification from './assets//notification-on.js'
import Users from './assets//users.js'
import Headphones from './assets//headphones.js'
import { SvgXml } from 'react-native-svg';


class LoginScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      step: 0,
      phoneNumber: ""
    };

    this.listItem = [
      {
        uri: Notification,
        title: 'Notification'
      },
      {
        uri: Users,
        title: 'Invite Friend'
      },
      {
        uri: Headphones,
        title: 'Help Center'
      },
      {
        uri: About_Us,
        title: 'About Us'
      },
      {
        uri: Log_Out,
        title: 'Log Out'
      },
    ]
  }

  renderItem = (uri, title) => {
    console.log("test")
    return(
      <View style={{flexDirection:'row',alignItems:'center'}}>
        <SvgXml xml={uri} />
        <Text title2 style={{margin:20}}>{title}</Text>
      </View> 
    )
  }

  render() {
    const {step} = this.state;
    return (
      <View style={styles.container}>
        <ScrollView>
        <View style={{flexDirection:'row', paddingTop:SCREEN_HEIGHT*0.08}}>
          <Image source={require('../../assets/local_image/Avatar.png')}  resizemode='contain' style={{alignSelf:'center', marginLeft:SCREEN_WIDTH*0.08}}></Image>
          <View style={{justifyContent:'center', alignContent:'center'}}>
            <Text title1 bold style={{ marginLeft:'10%', color:'#030A44'}}>Nhân Đặng</Text>
            <Text title3  style={{marginLeft:'10%', marginTop:'5%', color:'#030A44' }}>Ho Chi Minh City</Text>
          </View>
        </View>
        <View style={{flexDirection:'row', justifyContent:'center',borderColor:'#B8B8B8', borderWidth:2, alignItems:'center', marginTop: SCREEN_HEIGHT*0.04, paddingTop: 10, paddingBottom: 10}}>
          <View style={{width:SCREEN_WIDTH*0.5, height: SCREEN_HEIGHT*0.1, justifyContent: 'center', alignItems: 'center', borderRightWidth:1, borderColor: 'black', }}>
            <Text body1 bold  style={{alignSelf:'center', color:'#030A44', fontSize: 20}}>96.000 vnđ</Text>
            <Text body2 style={{alignSelf:'center',  color:'#030A44'}}>Total spending</Text>
          </View>
          <View style={{width:SCREEN_WIDTH*0.5}}>
          <Text body1 bold style={{alignSelf:'center', color:'#030A44', fontSize: 20}}>12</Text>
            <Text body2 style={{alignSelf:'center',  color:'#030A44'}}>Total Orders</Text>
          </View>
        </View >
        <View style={{justifyContent:'center', marginLeft:SCREEN_WIDTH*0.08 }}>
          {this.listItem.map(element => {
            return this.renderItem(element.uri, element.title)
          })}
        </View>
        </ScrollView>
      </View>
           );
    }
}



export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
 
});