import React, { Component } from 'react';
import PropTypes, { element } from 'prop-types';
import {
  StyleSheet,
  View,
  Animated,
  Dimensions,
  Image,
  Button,
  SafeAreaView,
  ScrollView,
  ImageBackground,

} from 'react-native';
import Utils from '../../helpers/Utils'
import Text from '../../components/Text'
import _ from "lodash"
import FirstBackground from './assets/first-background.js'
import FolderImg from './assets/folder.js'
import Stick from './assets/stick.js'
import Promotion1 from './assets/promote1.js'
import Promotion2 from './assets/promote2.js'
import Printer from './assets/printer.js'
import Photo from './assets/photo.js'
import DPRINTER from './assets/3dprint.js'
import More from './assets/more_horizontal.js'
import Svg, {
  SvgXml
} from 'react-native-svg';
const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  imgBanner: {
    width: '100%',
    height: 250,
    backgroundColor: '#5D6FFD',
    position: 'absolute',
    justifyContent: 'center',
    alignItems:'center',
  },
  rowBanner: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  userIcon: {
    width: 40,
    height: 40,
    borderRadius: 20,
    borderWidth: 1,
    borderColor: 'white',
    marginRight: 5,
  },
  contentButtonBottom: {
    borderTopWidth: 1,
    paddingVertical: 10,
    paddingHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  itemPrice: {
    borderBottomWidth: 1,
    paddingVertical: 10,
    alignItems: 'flex-start',
  },
  linePrice: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
  },
  iconRight: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  wrapContent: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    borderBottomWidth: 1,
    paddingBottom: 20,
  },
});

class LoginScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      password: "",
      locationReady: true,
      token: "",
      // userId: this.props.route.params.userRegistered.map(obj => obj.user._id),
      heightHeader: Utils.heightHeader(),
      savedDataLoaded: false,
      
    };
    
    this.listFolder = [
      {
        month: 'JUNE',
        day: '12',
        time: '13:20',
        file_name:'ASSIGNMENT of STRENGTH OF MATERIAL 1. STUDENT ',
      },
      {
        month: 'JULY',
        day: '28',
        time: '02:20',
        file_name:'BAO CAO HANG THANG CUA THANG 6',
      },
      {
        month: 'JULY',
        day: '28',
        time: '13:20',
        file_name:'ASSIGNMENT of MACHINE ELEMENT 1. STUDENT ',
      },
    ]
    this.listPromotions = [
      {
        Percentage:'-20%',
        Discount_name:'GREETING DISCOUNT',
        Discount_image: Promotion1
      },
      {
        Percentage:'-10%',
        Discount_name:'WEEKLY DISCOUNT',
        Discount_image: Promotion2
      },
    ]
  }

  renderFolder = (month, time, day, file_name) => {
    console.log('file')
    return(
      
      <View style={{borderBottomWidth:2, borderRightWidth:2, borderColor:'grey',borderRadius:20 , width:SCREEN_WIDTH*0.4, justifyContent:'center', alignItems:'center', marginLeft:10, padding:5}}>
        <View position='relative' >
          <SvgXml xml={FolderImg} style={{marginTop: 15}} />
          <View  style={{position:'absolute',top:'20%' }} >
            <SvgXml xml={Stick} style={{marginLeft:'7%'}}/>
            <Text body1 bold style={{borderBottomWidth:1,fontSize:12,  marginLeft:'5%', marginRight:'50%' }} >FILE NAME:</Text>
            <Text numberOfLines={2} style={{fontSize:12,  marginLeft:'5%', width:SCREEN_WIDTH*0.2}}>{file_name}</Text>
          </View>
        </View>
        <Text black style={{color:'#4B5FF7', fontSize:30}}>{month}</Text>
        <View style={{flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
            <Text black style={{marginTop:-10, color:'#4B5FF7'}} >{time}</Text>
            <Text light style={{color:'#4B5FF7', fontSize:30}}>{day}</Text>
        </View>
        
      </View>
      
    )
  }
 
  renderPromotions = (Percentage, Discount_name, Discount_image) => {
    
    return(
      <View style={{backgroundColor:'#8593FF', width:SCREEN_WIDTH*0.6,borderRadius:10, justifyContent:'center', alignItems:'center', marginLeft:10, padding:10, marginTop:'3%'}}>
        <SvgXml xml={Discount_image} width ={SCREEN_WIDTH*0.55} />
        <View style={{flexDirection:'row', justifyContent: 'center', alignItems:'center'}}>
    <Text black title1 style={{color:'white'}}	>{Percentage}</Text>
          <Text bold numberOfLines={2} style={{width:SCREEN_WIDTH*0.2, marginLeft:SCREEN_WIDTH*0.02, color:'white'}}>{Discount_name}</Text>
        </View>
      </View>
      
    )
}
  render() {
    const { username, password, savedDataLoaded, heightHeader} = this.state;

    return (
      <View style={styles.container}>

        <ScrollView
          scrollEventThrottle={8}
          >
            <View style= {{width: '100%', height: SCREEN_HEIGHT*0.3, justifyContent: 'center', alignItems: 'center', backgroundColor: '#5D6FFD'}}>
              <SvgXml xml={FirstBackground} style={{marginTop: 15}} height = '80%'/>
            </View>
            <View style ={{width: SCREEN_WIDTH, height: SCREEN_HEIGHT*0.25, alignItems: 'center'}}>
              <View style={{width: '90%', height: '100%', 
                          backgroundColor: 'white', 
                          top: -SCREEN_HEIGHT*0.02, 
                          borderRadius: 10,  
                          shadowColor: '#E9E9E9',
                          shadowOffset: { width: 10, height: 10 },
                          shadowOpacity: 1,
                          shadowRadius: 6,
                          paddingTop: SCREEN_HEIGHT*0.02,
                          paddingLeft: SCREEN_HEIGHT*0.03,
                          paddingRight: SCREEN_HEIGHT*0.03,

                          }}>
                <View style= {{width:'100%', height: SCREEN_HEIGHT*0.04, backgroundColor:'#E9E9E9', borderRadius: 10, justifyContent: 'center', alignItems: 'center'}}>
                  <Text body1 style={{fontSize: SCREEN_WIDTH*0.03}}>268 Ly Thuong Kiet, District 10, HCMC</Text>
                </View>
                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 20}}>
                  <View style={{width: SCREEN_WIDTH*0.15, height: SCREEN_WIDTH*0.15, backgroundColor: '#F4F4F4', borderWidth: 1, borderColor: '#E9E9E9', borderRadius: SCREEN_WIDTH*0.075, justifyContent:'center', alignItems:'center'}}>
                    <SvgXml xml={Printer}/>
                  </View>
                  <View style={{width: SCREEN_WIDTH*0.15, height: SCREEN_WIDTH*0.15, backgroundColor: '#F4F4F4', borderWidth: 1, borderColor: '#E9E9E9', borderRadius: SCREEN_WIDTH*0.075, justifyContent:'center', alignItems:'center'}}>
                    <SvgXml xml={Photo}/>
                  </View>
                  <View style={{width: SCREEN_WIDTH*0.15, height: SCREEN_WIDTH*0.15, backgroundColor: '#F4F4F4', borderWidth: 1, borderColor: '#E9E9E9', borderRadius: SCREEN_WIDTH*0.075, justifyContent:'center', alignItems:'center'}}>
                    <SvgXml xml={DPRINTER}/>
                  </View>
                  <View style={{width: SCREEN_WIDTH*0.15, height: SCREEN_WIDTH*0.15, backgroundColor: '#F4F4F4', borderWidth: 1, borderColor: '#E9E9E9', borderRadius: SCREEN_WIDTH*0.075, justifyContent:'center', alignItems:'center'}}>
                    <SvgXml xml={More}/>
                  </View>
                </View>
              </View>
            </View>
            <View>
              <ScrollView horizontal={true} style={{ marginLeft:SCREEN_WIDTH*0.02 }}>
            
                   {this.listFolder.map(element => {
                    return this.renderFolder(element.month, element.day, element.time, element.file_name)
                     })}
                    <View style={{width:SCREEN_WIDTH*0.03 }}></View>
           
              </ScrollView>
            </View>
            <View>
              <ScrollView horizontal={true} style={{ marginLeft:SCREEN_WIDTH*0.02 }}>
            
                   {this.listPromotions.map(element => {
                    return this.renderPromotions(element.Percentage, element.Discount_name, element.Discount_image)
                     })}
           
              </ScrollView>
            </View> 
            
                    
        </ScrollView>
        <View style={{height:SCREEN_WIDTH*0.08}}></View>
      </View>
    );
  }
}



export default LoginScreen;
