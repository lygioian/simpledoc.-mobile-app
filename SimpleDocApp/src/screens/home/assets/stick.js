export default svg = `<svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
<g filter="url(#filter0_d)">
<path d="M14 20C19.5228 20 24 15.5228 24 10C24 4.47715 19.5228 0 14 0C8.47715 0 4 4.47715 4 10C4 15.5228 8.47715 20 14 20Z" fill="#35DC86"/>
<path d="M11.6923 14.6152C11.4883 14.6151 11.2927 14.5341 11.1485 14.3898L8.84078 12.0821C8.70066 11.937 8.62312 11.7427 8.62488 11.541C8.62663 11.3393 8.70753 11.1464 8.85015 11.0038C8.99277 10.8611 9.18571 10.7803 9.38739 10.7785C9.58908 10.7767 9.78339 10.8543 9.92847 10.9944L11.6923 12.7582L18.0715 6.37902C18.2166 6.2389 18.4109 6.16136 18.6126 6.16312C18.8143 6.16487 19.0073 6.24577 19.1499 6.38839C19.2925 6.53101 19.3734 6.72394 19.3751 6.92563C19.3769 7.12732 19.2994 7.32163 19.1592 7.46671L12.2362 14.3898C12.0919 14.5341 11.8963 14.6151 11.6923 14.6152Z" fill="#F1F2F2"/>
</g>
<defs>
<filter id="filter0_d" x="0" y="0" width="28" height="28" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
<feOffset dy="4"/>
<feGaussianBlur stdDeviation="2"/>
<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"/>
<feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
<feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
</filter>
</defs>
</svg>`
