export default svg = `<svg width="103" height="104" viewBox="0 0 103 104" fill="none" xmlns="http://www.w3.org/2000/svg">
<g filter="url(#filter0_d)">
<path d="M82.3456 25.7336H47.2962V13.3633H80.2838C80.5562 13.3574 80.8269 13.4067 81.0798 13.5082C81.3326 13.6097 81.5622 13.7613 81.7548 13.954C81.9475 14.1466 82.0991 14.3763 82.2007 14.6291C82.3022 14.8819 82.3515 15.1526 82.3456 15.425V25.7336Z" fill="#FFCB15"/>
</g>
<g filter="url(#filter1_d)">
<path d="M47.2962 13.3635V25.7339H90.5925C92.7797 25.7339 94.8773 26.6027 96.4239 28.1493C97.9705 29.6959 98.8394 31.7936 98.8394 33.9808V87.5856C98.8394 89.7729 97.9705 91.8705 96.4239 93.4171C94.8773 94.9637 92.7797 95.8325 90.5925 95.8325H12.2469C10.0597 95.8325 7.96206 94.9637 6.41546 93.4171C4.86887 91.8705 4 89.7729 4 87.5856V9.24007C4 9.24007 4 0.993164 12.2469 0.993164H39.0493C39.0493 0.993164 47.2962 0.993164 47.2962 9.24007V13.3635Z" fill="#FFDC62"/>
</g>
<defs>
<filter id="filter0_d" x="43.2962" y="13.3628" width="43.0498" height="20.3708" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
<feOffset dy="4"/>
<feGaussianBlur stdDeviation="2"/>
<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"/>
<feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
<feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
</filter>
<filter id="filter1_d" x="0" y="0.993164" width="102.839" height="102.839" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
<feOffset dy="4"/>
<feGaussianBlur stdDeviation="2"/>
<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"/>
<feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
<feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
</filter>
</defs>
</svg>`
