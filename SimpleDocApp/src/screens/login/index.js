import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
  StyleSheet,
  View,
  Image,
  Dimensions,
  Text,
  TextInput,
  TouchableOpacity
} from 'react-native';


const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

import Logo from '../../assets/local_image/Logo.png'
import Box from '../../assets/local_image/Box.png'

class LoginScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      step: 0,
      phoneNumber: ""
    };
  }
  sendingOTP = () => 
          {
    this.setState({step: 1});
          }
  
  render() {
    const {step} = this.state;
    return (
      <View style={{paddingTop:'20%'}}>
          <Image source={require('../../assets/local_image/Logo.png')}  resizemode='contain' style={{width:'25%', alignSelf:'center'}}></Image>
          <Image source={require('../../assets/local_image/Box.png')}  resizemode='contain' style={{width:'50%', alignSelf:'center', marginTop:'10%'}}></Image>
          <Text style={{color:'#0C2B6C', fontSize:15 , marginTop:'10%', marginLeft:'10%'}}>YOUR PHONE NUMBER</Text>
          <View style={{justifyContent:'center',  marginLeft:'10%', marginRight:'10%', flexDirection:'row', height:'12%', marginTop:'5%'}}>
            <View style={{ flex:1,flexDirection:'row', borderColor:'rgba(12,43,108,0.5)', borderWidth:2, borderRadius:10, justifyContent:'center', alignItems:'center'}}>
              <Image source={require('../../assets/local_image/flag.png')} resizemode='contain' style={{height:'55%', width:'45%', borderRadius:3}}></Image>
              <Text style={{ alignSelf:'center', marginLeft:'5%', fontSize:22, color:'#0C2B6C'}}>+84</Text>
            </View>
            <View style={{ flex:2, borderColor:'rgba(12,43,108,0.5)', borderWidth:2, marginLeft:'5%', borderRadius:10, justifyContent:'center'}}>
                <TextInput 
                placeholder='918 123 456'
                placeholderTextColor='rgba(12,43,108,0.5)'
                keyboardType='numeric'
                style={{ marginLeft:'5%', fontSize:22, color:'#0C2B6C'}}>
                </TextInput>
            </View>
          </View>
          <TouchableOpacity onPress={()=> this.sendingOTP} style={{backgroundColor:'#4B5FF7', width:'30%', height:'10%',borderRadius:5, marginTop:'10%', marginright:'25%',justifyContent:'center', alignItems:'center', marginLeft:'60%'}}>
            <Text style={{color:'#FFFFFF', fontWeight:'bold', fontSize:20}}>NEXT</Text>
          </TouchableOpacity>
         
      </View>
           );
    }
}



export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
 
});