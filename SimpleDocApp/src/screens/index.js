import React from "react";

import LoginScreen from './login';
import HomeScreen from './home';
import HistoryScreen from './history';
import QRScreen from './qr';
import NotifyScreen from './notify';
import ProfileScreen from './profile';


export const Login = (navigation) => <LoginScreen {...navigation} name="Login" />;
export const Home = (navigation) => <HomeScreen {...navigation} name="Home" />;
export const History = (navigation) => <HistoryScreen {...navigation} name="History" />;
export const QR = (navigation) => <QRScreen {...navigation} name="QR" />;
export const Notify = (navigation) => <NotifyScreen {...navigation} name="Notify" />;
export const Profile = (navigation) => <ProfileScreen {...navigation} name="Profile" />;