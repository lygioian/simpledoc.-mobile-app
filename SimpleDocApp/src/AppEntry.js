import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { Router } from "./router";
import {initializeStore} from "./redux"

const store = initializeStore({})

export default class App extends Component {
    render() {
      return (
        <Provider store={store}>
            <Router />
        </Provider>
      );
    }
  }